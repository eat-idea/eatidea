package pl.eatidea.api.meal.transport.domain;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.meal.model.builder.MealBuilder;
import pl.eatidea.api.meal.transport.MealDto;
import pl.eatidea.api.meal.transport.MealDtoDetails;
import pl.eatidea.api.meal.transport.MealDtoDetailsBuilder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MealTransformerTest {

	private Meal testMeal;
	private MealDtoDetails testMealDtoDetails;
	private final Long ID = 1L;
	private final String INGREDIENTS_STRING = "ing1;ing2";
	private final String NAME = "testName";
	private final List<String> INGREDIENTS_LIST = Lists.newArrayList("ing1", "ing2");
	private final Integer PREPARE_TIME = 666;
	private final String RECEIPT = "Nothing to do";

	@Before
	public void clearMeals() {
		testMeal = MealBuilder.aMeal()
			.withId(ID)
			.withIngredients(INGREDIENTS_STRING)
			.withName(NAME)
			.withPrepareTime(PREPARE_TIME)
			.withReceipt(RECEIPT)
			.build();

		testMealDtoDetails = MealDtoDetailsBuilder.aMealDtoDetails()
			.withId(ID)
			.withIngredients(INGREDIENTS_LIST)
			.withName(NAME)
			.withPrepareTime(PREPARE_TIME)
			.withReceipt(RECEIPT)
			.build();
	}


	@Test
	public void shouldTransformMealToMealDto() {
		//when
		MealDto dto = MealTransformer.MealToMealDto.apply(testMeal);

		//then
		assertThat(dto.getId()).isEqualTo(ID);
		assertThat(dto.getName()).isEqualTo(NAME);
		assertThat(dto.getPrepareTime()).isEqualTo(PREPARE_TIME);
	}

	@Test
	public void shouldTransformMealToMealDtoDetails() {
		//when
		MealDtoDetails dto = MealTransformer.MealToMealDtoDetails.apply(testMeal);

		//then
		assertThat(dto.getId()).isEqualTo(ID);
		assertThat(dto.getIngredients()).isEqualTo(INGREDIENTS_LIST);
		assertThat(dto.getName()).isEqualTo(NAME);
		assertThat(dto.getPrepareTime()).isEqualTo(PREPARE_TIME);
		assertThat(dto.getReceipt()).isEqualTo(RECEIPT);
	}

	@Test
	public void shouldTransformMealDtoDetailsToMeal() {
		//when
		Meal meal = MealTransformer.MealDtoDetailsToMeal.apply(testMealDtoDetails);

		//then
		assertThat(meal.getId()).isEqualTo(ID);
		assertThat(meal.getIngredients()).isEqualTo(INGREDIENTS_STRING);
		assertThat(meal.getName()).isEqualTo(NAME);
		assertThat(meal.getPrepareTime()).isEqualTo(PREPARE_TIME);
		assertThat(meal.getReceipt()).isEqualTo(RECEIPT);
	}


}
