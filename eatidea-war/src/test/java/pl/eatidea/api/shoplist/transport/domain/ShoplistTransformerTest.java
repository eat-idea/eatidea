package pl.eatidea.api.shoplist.transport.domain;

import org.assertj.core.util.Lists;
import org.junit.Test;
import pl.eatidea.api.shoplist.transport.ShoplistDto;
import pl.eatidea.shoplist.model.Shoplist;
import pl.eatidea.shoplist.model.builder.ShoplistBuilder;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class ShoplistTransformerTest {

	@Test
	public void should_transform_shoplist_to_dto() {
		//given
		Date now = new Date();
		Shoplist shoplist = ShoplistBuilder.aShoplist()
			.withId(1L)
			.withName("Amama")
			.withCreateDate(now)
			.withProducts("abc;bcd").build();

		//when
		ShoplistDto dto = ShoplistTransformer.ShoplistToShoplistDto.apply(shoplist);

		//then
		assertThat(dto.getId()).isEqualTo(shoplist.getId());
		assertThat(dto.getName()).isEqualTo(shoplist.getName());
		assertThat(dto.getCreateDate()).isEqualTo(now);
		assertThat(dto.getProducts()).isEqualTo(Lists.newArrayList("abc", "bcd"));
	}
}
