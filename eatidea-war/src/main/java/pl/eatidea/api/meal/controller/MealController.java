package pl.eatidea.api.meal.controller;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.eatidea.api.common.ApiController;
import pl.eatidea.api.meal.transport.MealDto;
import pl.eatidea.api.meal.transport.MealDtoDetails;
import pl.eatidea.api.meal.transport.domain.MealTransformer;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.meal.application.MealServiceImpl;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.shoplist.application.ShoplistService;
import pl.eatidea.shoplist.model.Shoplist;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Meal web api
 */
@Controller
public class MealController extends ApiController {

    private final Long createTimeDelayInMs = 30000L;//30sec

    @Autowired
    MealServiceImpl mealService;

    @Autowired
    Logger logger;

    @Autowired
    ShoplistService shoplistService;

    /**
     * Meal list
     * @return
     */
    @RequestMapping(value = "/meal", method = RequestMethod.GET)
    @ResponseBody
    public List<MealDto> getList() {
        List<Meal> meals = mealService.getMeals();
        List<MealDto> mealsDto = Lists.transform(meals, MealTransformer.MealToMealDto);
        return mealsDto;
    }

    /**
     * Meal details
     * @param id
     * @return
     * @throws ApplicationException
     */
    @RequestMapping(value="/meal/{id}", method = RequestMethod.GET)
    @ResponseBody
    public MealDtoDetails getMeal(@PathVariable Long id) throws ApplicationException {
        logger.error("Hello");
        return  MealTransformer.MealToMealDtoDetails.apply(mealService.getMeal(id));
    }

    /**
     * Meal save
     * @param request
     * @param response
     * @param dto
     * @throws ApplicationException
     */
    @RequestMapping(value = "/meal", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void saveMeal(HttpServletRequest request, HttpServletResponse response, @RequestBody MealDtoDetails dto) throws ApplicationException {
        Long now = System.currentTimeMillis();
        Cookie cookies[] = request.getCookies() != null ? request.getCookies() : new Cookie[0];
        Optional<Cookie> lastCreateCookie = Arrays.stream(cookies).filter(c -> c.getName().equals("saveTime")).findFirst();

        if(dto.getName() == null || dto.getName().equals("")) {
            throw new ApplicationException("Pole name musi być wypełnione");
        }

        if(lastCreateCookie.isPresent()) {
            try {
                Long delay = now - Long.valueOf(lastCreateCookie.get().getValue());
                if (delay < createTimeDelayInMs) {
                    throw new ApplicationException("Odczekaj " + createTimeDelayInMs/1000 + " sekund zanim utworzysz nowy przepis. Pozostało: " + (createTimeDelayInMs-delay)/1000  + " sekund");
                }
            }
            catch (NumberFormatException ex) {
                //log invalidCookieValue occurred
            }
        }

        mealService.saveMeal(MealTransformer.MealDtoDetailsToMeal.apply(dto));

        response.addCookie(new Cookie("saveTime", now.toString()));
    }

    /**
     * Delete meal
     * @param id
     */
    @RequestMapping(value = "/meal/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteMeal(@PathVariable Long id) {
        mealService.deleteMeal(id);
    }

    /**
     * Creating/adding shoplist for meal
     * @param id - mealId
     * @param name - optional shoplist name
     * @param parentShoplistId - optional shoplist id for add products
     * @return
     */
    @RequestMapping(value= "/meal/{id}/shoplist", method = RequestMethod.POST)
    @ResponseBody
    public Long createShoplist(@PathVariable Long id, @RequestParam(required = false) String name, @RequestParam(required = false) Long parentShoplistId) {
        Shoplist shoplist;
        if(parentShoplistId != null) {
            shoplist = shoplistService.addMealToShoplist(parentShoplistId, id);
        }
        else {
            shoplist = shoplistService.createShoplistForMeal(name, id);
        }

        return shoplist.getId();
    }
}
