package pl.eatidea.api.meal.transport.domain;

import com.google.common.base.Function;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.meal.model.builder.MealBuilder;
import pl.eatidea.api.meal.transport.MealDto;
import pl.eatidea.api.meal.transport.MealDtoBuilder;
import pl.eatidea.api.meal.transport.MealDtoDetails;
import pl.eatidea.api.meal.transport.MealDtoDetailsBuilder;

import java.util.Arrays;
import java.util.List;

/**
 * Meat transformer
 * Dto's to Entity & Entity to Dto's
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class MealTransformer {

    /**
     * Meal to MealDto
     */
    public static Function<Meal, MealDto> MealToMealDto = meal ->
        MealDtoBuilder.aMealDto().withId(meal.getId())
		.withName(meal.getName()).withPrepareTime(meal.getPrepareTime()).build();

    /**
     * Meal to MealDetailsDto
     */
    public static Function<Meal, MealDtoDetails> MealToMealDtoDetails = meal -> {

        List<String> ingredients = Arrays.asList(meal.getIngredients().split(";"));
        return MealDtoDetailsBuilder.aMealDtoDetails()
            .withId(meal.getId())
            .withIngredients(ingredients)
            .withName(meal.getName())
            .withPrepareTime(meal.getPrepareTime())
            .withReceipt(meal.getReceipt()).build();
    };

    /**
     * MealDetailsDto to Meal
     */
    public static Function<MealDtoDetails, Meal> MealDtoDetailsToMeal = dto -> {
        String ingredients = String.join(";", dto.getIngredients());

        return MealBuilder.aMeal().withId(dto.getId())
            .withReceipt(dto.getReceipt())
            .withPrepareTime(dto.getPrepareTime())
            .withName(dto.getName())
            .withIngredients(ingredients).build();
    };
}
