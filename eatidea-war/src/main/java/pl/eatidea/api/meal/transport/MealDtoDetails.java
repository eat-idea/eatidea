package pl.eatidea.api.meal.transport;

import java.util.List;

/**
 * Meal Details Data Transfer Object
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class MealDtoDetails {

    private Long id;
    private String name;
    private List<String> ingredients;
    private String receipt;
    private Integer prepareTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Integer getPrepareTime() {
        return prepareTime;
    }

    public void setPrepareTime(Integer prepareTime) {
        this.prepareTime = prepareTime;
    }
}
