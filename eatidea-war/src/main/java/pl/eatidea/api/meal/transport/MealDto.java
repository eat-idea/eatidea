package pl.eatidea.api.meal.transport;

/**
 * Meal Data Transfer Object
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class MealDto {

    private Long id;
    private String name;
    private Integer prepareTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrepareTime() {
        return prepareTime;
    }

    public void setPrepareTime(Integer prepareTime) {
        this.prepareTime = prepareTime;
    }
}
