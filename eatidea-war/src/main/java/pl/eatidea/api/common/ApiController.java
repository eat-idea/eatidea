package pl.eatidea.api.common;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ApiController
 * that have prefix request mapping: /api
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@RequestMapping("/api")
public abstract class ApiController {
}
