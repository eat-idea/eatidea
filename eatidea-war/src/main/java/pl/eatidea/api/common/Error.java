package pl.eatidea.api.common;

import java.sql.Timestamp;

/**
 * Error Data Travel Object
 */
public class Error {

    private Integer code;

    private Long timestamp;

    private String description;

    public Error() {
    }

    public Error(Integer code, String description) {
        this(code, new Timestamp(System.currentTimeMillis()).getTime(), description);
    }

    public Error(Integer code, Long timestamp, String description) {
        this.code = code;
        this.timestamp = timestamp;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
