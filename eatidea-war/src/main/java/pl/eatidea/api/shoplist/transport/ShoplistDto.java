package pl.eatidea.api.shoplist.transport;

import java.util.Date;
import java.util.List;

/**
 * Shoplist Data Tranfer Object
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class ShoplistDto {

    private Long id;

    private String name;

    private List<String> products;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
