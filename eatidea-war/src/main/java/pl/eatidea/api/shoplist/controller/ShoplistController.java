package pl.eatidea.api.shoplist.controller;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.eatidea.api.common.ApiController;
import pl.eatidea.api.shoplist.transport.ShoplistDto;
import pl.eatidea.api.shoplist.transport.domain.ShoplistTransformer;
import pl.eatidea.shoplist.application.ShoplistService;

import java.util.List;

/**
 * Shoplist api
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@RestController
public class ShoplistController extends ApiController {

    @Autowired
    ShoplistService shoplistService;

    /**
     * shoplist list
     * @return
     */
    @RequestMapping(value = "/shoplist", method = RequestMethod.GET)
    @ResponseBody
    public List<ShoplistDto> getLists() {
        return Lists.transform(shoplistService.getShoplists(), ShoplistTransformer.ShoplistToShoplistDto);
    }

    /**
     * shoplsit detalis
     * @param id
     * @return
     */
    @RequestMapping(value ="/shoplist/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ShoplistDto getShoplist(@PathVariable Long id) {
        return ShoplistTransformer.ShoplistToShoplistDto.apply(shoplistService.getShoplist(id));
    }


    @RequestMapping(value ="/shoplist/{id}/removeProduct", method = RequestMethod.GET)
    @ResponseBody
    public ShoplistDto removeProductFromShoplist(@PathVariable Long id, @RequestParam("name") String product) {
        return ShoplistTransformer.ShoplistToShoplistDto.apply(shoplistService.removeProductFromShoplist(id, product));
    }
}
