package pl.eatidea.api.shoplist.transport.domain;

import com.google.common.base.Function;
import pl.eatidea.api.shoplist.transport.ShoplistDto;
import pl.eatidea.api.shoplist.transport.ShoplistDtoBuilder;
import pl.eatidea.shoplist.model.Shoplist;

import java.util.Arrays;

/**
 * Shoplist Entity/dto transformer
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class ShoplistTransformer {

    public static Function<Shoplist, ShoplistDto> ShoplistToShoplistDto = shoplist -> ShoplistDtoBuilder.aShoplistDto().withId(shoplist.getId())
		.withName(shoplist.getName())
		.withCreateDate(shoplist.getCreateDate())
		.withProducts(Arrays.asList(shoplist.getProducts().split(";")))
		.build();

}
