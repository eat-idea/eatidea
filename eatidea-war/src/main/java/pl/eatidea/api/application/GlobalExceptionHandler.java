package pl.eatidea.api.application;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.api.common.Error;

/**
 * Global Controller Exception Handler
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handler for ApplicationException
     * @param e
     * @return
     */
    @ExceptionHandler(value = ApplicationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Error handleApplicationError(Exception e) {
        e.printStackTrace(); //for developing
        return new Error(2, e.getMessage());
    }

    /**
     * Handler for other Exceptions
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Error handleError(Exception e) {
        e.printStackTrace(); //for developing
        return new Error(1, "SERVER ERROR");
    }

}
