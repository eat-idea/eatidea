EatIdeaApp.Routers.ShoplistRouter = [
    {
        when: "/shoplist",
        templateUrl: "app/shoplist/views/shoplist.html",
        controllerJs: "app/shoplist/controllers/ShoplistController.js",
        controller: "ShoplistController"
    },
    {
        when: "/shoplist/:id",
        templateUrl: "app/shoplist/views/shoplistDetails.html",
        controllerJs: "app/shoplist/controllers/ShoplistController.js",
        controller: "ShoplistDetailsController"
    }
];