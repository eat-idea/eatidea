EatIdeaApp.controller('ShoplistController', function($scope, $http, $location, toastr) {

    $http.get("/api/shoplist")
        .then(function (response) {
            $scope.shoplists = response.data;
        });

    $scope.showDetails = function(id) {
        $location.path('/shoplist/'+ id)
    }



});

EatIdeaApp.controller('ShoplistDetailsController', function($scope, $http, toastr, $routeParams, $location) {

    $scope.id = $routeParams.id;

    var refresh = function() {
        $http.get("/api/shoplist/" + $scope.id)
            .then(function (response) {
                $scope.shoplist = response.data;
            });
    };

    $scope.removeProduct = function(name) {
        $http.get("/api/shoplist/" + $scope.id + "/removeProduct?name=" + name)
            .then(function (response) {
                refresh();
            }, function (reason) {
            });
    };

    refresh();
});