EatIdeaApp.Routers.HomeRouter = [
    {
        when: "/",
        templateUrl: "app/home/views/home.html",
        controllerJs: "app/home/controllers/HomeController.js",
        controller: "HomeController"
    }
];