
EatIdeaApp.controller('IndexController', function($scope, $window) {

    $scope.menuSidebarHide = function() {
        document.getElementById("menuSidebar").style.display = "none";
        document.getElementById("pageContent").style.marginLeft = "0%";
        document.getElementById("openNav").style.display = "inline-block";
    };

    $scope.menuSidebarShow = function() {
        document.getElementById("menuSidebar").style.display = "block";
        document.getElementById("pageContent").style.marginLeft = "25%";
        document.getElementById("openNav").style.display = 'none';
    };

    //chowanie sideMenu gdy zajmuje cały ekran
    $scope.hideMenuOnMobile = function() {
        if($window.innerWidth <= 600) {
            $scope.menuSidebarHide();
        }
    };
});