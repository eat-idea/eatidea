var App = angular.module("App", [
    "ui.bootstrap",
    "App.Controllers",
    "App.Routers",
    "App.Services",
    "App.Filters",
    "App.Directives",
    "ngRoute"
]);

App.Version = "1.0.0";

App.Controllers = angular.module("App.Controllers", []);
App.Routers = angular.module("App.Routers", []);
App.Services = angular.module("App.Services", []);
App.Filters = angular.module("App.Filters", []);
App.Directives = angular.module("App.Directives", []);
App.Languages = {};

App.Session = sessionStorage;
App.Storage = localStorage;

App.injector = angular.injector([ "ng" ]);

/**
 * Metody
 */
App.Class = function(_o) {
    var o = function() {
        _o.init.apply(this, arguments);
    };
    o.prototype = _o;

    return o;
};

App.require = function(object, async, success) {

    var file = object.split('.');

    var type = file[1].substring(0, file[1].length - 1);
    var module = null;
    var targetFile = null;

    if (type == 'Language') {
        file = "./" + file.slice(0, 2).join('/').toLowerCase() + "/" + file[2] + ".js";

    } else {
        if (type == 'Router') {
            module = file[2].replace(type, '').toLowerCase();

        } else {
            module = file[2].toLowerCase();
        }

        targetFile = file[1].toLowerCase() + "/" + file[2];
        if (file[3] !== undefined && (endsWith(file[3], 'Controller') || endsWith(file[3], 'Service') || endsWith(file[3], 'Router'))) {
            targetFile += file[3];
        }


        file = "./" + file[0].toLowerCase() + "/" + module + "/" + targetFile + ".js";
    }

    $.ajax({
        url: file,
        type: "GET",
        async: async || false,
        dataType: "script",
        cache: true,
        success: function () {
            if (typeof success != "undefined") {
                success();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            throw ("[App] Can't load object " + object);
        }
    });
};

// ROUTING
App.run(function($rootScope, $route, $location, $routeParams, $log, $http) {
    $rootScope.$on('$locationChangeStart', function(ev, next, current) {
        var module = next.match(/#!([^\/\?.]*)/)

        module = module[1] ? module[1] : "";

        var configFile;
        if(module != "") {
            module = module.toLowerCase();
            module = module.charAt(0).toUpperCase() + module.slice(1);
            configFile = "./app/"+module.toLowerCase()+"/config/"+module+"Config.js";
        }
        else {
            //#! - index path
            //indexConfig
            configFile = "./app/index/config/IndexConfig.js";
        }

        $http({
            method : "GET",
            url : configFile,
            cache: true,
            dataType: "script"
        }).then(function mySuccess(response) {
            //nothing
        }, function myError(response) {
            throw ("[App] Can't load object " + configFile);
        });
    });
});