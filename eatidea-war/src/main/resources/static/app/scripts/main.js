var EatIdeaApp = angular.module('eatIdeaApp', [
    "ui.bootstrap",
    "ngRoute",
    'ngAnimate',
    'toastr',
    'EatIdeaApp.Routers'
]);

EatIdeaApp.Routers = angular.module("EatIdeaApp.Routers", []);

EatIdeaApp.require = function(o, async, successCallback) {
    var eatIdeaAppStr = "EatIdeaApp";
    var file = o;
    if(o.substring(0, eatIdeaAppStr.length) === eatIdeaAppStr) {
        var parts = o.split(".");
        var type = parts[1]; //Routers / Controllers
        var module = parts[2].split("/(?=[A-Z])/")[0].toLowerCase();

        file="./app/" + module + "/";
    }

    $.ajax({
        url : file,
        type : "GET",
        async : async || false,
        dataType : "script",
        cache : true,
        success : function() {
            if (typeof successCallback != "undefined") {
                successCallback();
            }
        },
        error : function(xhr, ajaxOptions, thrownError) {
            throw ("[App] Can't load object " + o);
        }
    });
};

EatIdeaApp.resolve = function(object) {
    return {
        load : ['$q', '$rootScope', function($q, $rootScope) {
            var deferred = $q.defer();
            EatIdeaApp.require(object, true, function() {
                $rootScope.$apply(function() {
                    deferred.resolve();
                });
            });
            return deferred.promise;
        }]
    };
};

EatIdeaApp.Routers.addRoute = function(routing) {
    if(typeof routing.controllerJs != 'undefined') {
        routing.resolve = EatIdeaApp.resolve(routing.controllerJs);
    }

    EatIdeaApp.Routers.provider.when(routing.when, {
        templateUrl: routing.templateUrl,
        controller: routing.controller,
        resolve: routing.resolve
    }).otherwise({
        redirectTo: '/'
    });

    return this;
};

EatIdeaApp.config(['$httpProvider', '$routeProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', 'toastrConfig',
    function($httpProvider, $routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, toastrConfig) {

        EatIdeaApp.controller = $controllerProvider.register;
        EatIdeaApp.directive = $compileProvider.directive;
        EatIdeaApp.filter = $filterProvider.register;
        EatIdeaApp.factory = $provide.factory;
        EatIdeaApp.service = $provide.service;
        EatIdeaApp.constant = $provide.constant;
        EatIdeaApp.value = $provide.value;

    EatIdeaApp.Routers.provider = $routeProvider;

    //
    // $routeProvider
    //     .when('/', {
    //         templateUrl : 'app/home/views/home.html',
    //         controller  : 'homeController'
    //     })
    //
    //     // .when('/meal', {
    //     //     templateUrl : 'app/meal/views/meal.html',
    //     //     controller  : 'mealController'
    //     // })
    //     //
    //     // .when('/meal/:id', {
    //     //     templateUrl : 'app/meal/views/mealDetail.html',
    //     //     controller  : 'mealDetailController'
    //     // })
    //
    //     .when('/shoplist', {
    //         templateUrl : 'app/shoplist/views/shoplist.html',
    //         controller  : 'shoplistController'
    //     })
    //
    //     .when('/shoplist/:id', {
    //         templateUrl : 'app/shoplist/views/shoplistDetails.html',
    //         controller  : 'shoplistDetailsController'
    //     });

    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 4,
        closeButton: true,
        newestOnTop: true,
        timeOut: 2000,
        extendedTimeOut: 5000,
        positionClass: 'toast-bottom-right',
        progressBar: true,
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });

    $httpProvider.interceptors.push(function($q, $injector) {
        return {
            'responseError': function(rejection) {
                var title = 'Przepraszamy';
                var msg = 'Nie mogliśmy przetworzyć twojego żądania :(';
                var custom = {};

                if(rejection.data != undefined && rejection.data.code == 2) {
                    msg = rejection.data.description;
                    custom.timeOut = 7000;
                }

                $injector.invoke(function(toastr) {
                    toastr.error(msg, title, custom);
                });

                return $q.reject(rejection);
            }
        };
    });
}]);

// ROUTING
EatIdeaApp.run(function($rootScope, $route, $location, $routeParams, $log, $http) {
    $rootScope.$on('$locationChangeStart', function(ev, next, current) {
        var module = next.match(/(?:[^#.]*)#!\/([^\/?.]*)/);

        module = module ? module : "";
        module = module[1] ? module[1] : "";

        var routeFile;
        if(module != "") {
            module = module.toLowerCase();
            module = module.charAt(0).toUpperCase() + module.slice(1);
            routeFile = "./app/"+module.toLowerCase()+"/routers/"+module+"Router.js";
        }
        else {
            //default home router
            module = "Home";
            routeFile = "./app/home/routers/HomeRouter.js";
        }

        //require module
        EatIdeaApp.require(routeFile);
        //add routing
        var routing = EatIdeaApp.Routers[module+"Router"];
        if (typeof (routing) != 'undefined') {
            for (var i = 0; i < routing.length; i++) {
                EatIdeaApp.Routers.addRoute(routing[i]);
            }
        }

        //add index controller
        EatIdeaApp.require("./app/index/controllers/IndexController.js");
        $route.reload();
    });

});

//
// EatIdeaApp.controller('mealController', function($scope, $http, $location, toastr, $window) {
//
//     var refreshMeals = function() {
//         $http({
//             method : "GET",
//             url : "/api/meal"
//         }).then(function mySuccess(response) {
//             $scope.meals = response.data;
//         }, function myError(response) {
//             $scope.meals = undefined;
//         });
//
//         $scope.newMeal = {
//             name: undefined,
//             ingredients: [],
//             receipt: undefined,
//             prepareTime: undefined
//         };
//     };
//
//
//
//
//     $scope.blur = function( $event, $index ){
//         $scope.newMeal.ingredients[ $index ] = $event.currentTarget.value;
//     };
//
//     $scope.newIngredient = function() {
//         $scope.newMeal.ingredients.push("");
//     };
//
//     $scope.removeIngredient = function(id) {
//         $scope.newMeal.ingredients.splice(id, 1);
//     };
//
//     $scope.details = function(id) {
//         $location.path('/meal/' + id);
//     };
//
//     $scope.addMeal = function() {
//
//         $http.post("/api/meal", JSON.stringify($scope.newMeal)).then(function (response) {
//             toastr.success('We added your meal to list :)', 'Success');
//             refreshMeals();
//         });
//     };
//
//     refreshMeals();
// });
//
// EatIdeaApp.controller('mealDetailController', function($scope, $http, $routeParams, $location, toastr) {
//
//     $http.get("/api/meal/" + $routeParams.id)
//         .then(function mySuccess(response) {
//             $scope.meal = response.data;
//     }, function myError(response) {
//         $scope.meal = undefined;
//     });
//
//     $scope.deleteMeal = function(id){
//         $http({
//             method : "DELETE",
//             url : "/api/meal/" + id
//         }).then(function mySuccess(response) {
//             toastr.success("Usunięto");
//             $location.path('/meal');
//         }, function myError(response) {
//
//         });
//     };
//
//     $scope.vars = {};
//     $scope.vars.shoplistName = "";
//
//     $scope.createShoplist = function() {
//         $("#selectNameModal").css('display', 'none');
//         $http.post("/api/meal/"+$scope.meal.id+"/shoplist?name="+ $scope.vars.shoplistName)
//             .then(function (response) {
//                 toastr.info("Kliknij aby przejść do listy", "Utworzono liste zakupow", {
//                     onTap: function() {
//                         $location.path('/shoplist/' + response.data);
//                     }
//                 });
//             });
//     };
//
//     $scope.showNameSelect = function() {
//         $("#selectNameModal").css('display', 'block');
//
//     };
//
//     $scope.showShoplists = function () {
//         $("#shoplistsModal").css('display', 'block');
//
//         $http.get("/api/shoplist")
//             .then(function (response) {
//                 $scope.ableShoplists = response.data;
//             }, function (reason) {
//                 $scope.ableShoplists = [];
//             });
//     };
//
//     $scope.addToShoplist = function(id) {
//         $("#shoplistsModal").css('display', 'none');
//         $http.post("/api/meal/"+$scope.meal.id+"/shoplist?parentShoplistId="+id)
//             .then(function (response) {
//                 toastr.info("Kliknij aby przejść do listy", "Utworzono liste zakupow", {
//                     onTap: function() {
//                         $location.path('/shoplist/' + response.data);
//                     }
//                 });
//             });
//     };
//
// });
//
// EatIdeaApp.controller('shoplistController', function($scope, $http, $location, toastr) {
//
//     $http.get("/api/shoplist")
//         .then(function (response) {
//             $scope.shoplists = response.data;
//         });
//
//     $scope.showDetails = function(id) {
//         $location.path('/shoplist/'+ id)
//     }
//
//
//
// });
//
// EatIdeaApp.controller('shoplistDetailsController', function($scope, $http, toastr, $routeParams, $location) {
//
//     $scope.id = $routeParams.id;
//
//     var refresh = function() {
//         $http.get("/api/shoplist/" + $scope.id)
//             .then(function (response) {
//                 $scope.shoplist = response.data;
//             });
//     };
//
//     $scope.removeProduct = function(name) {
//         $http.get("/api/shoplist/" + $scope.id + "/removeProduct?name=" + name)
//             .then(function (response) {
//                 refresh();
//             }, function (reason) {
//             });
//     };
//
//     refresh();
// });


