EatIdeaApp.controller('MealController', function($scope, $http, $location, toastr, $window) {

    $scope.abc = "cba";

    var refreshMeals = function() {
        $http({
            method : "GET",
            url : "/api/meal"
        }).then(function mySuccess(response) {
            $scope.meals = response.data;
        }, function myError(response) {
            $scope.meals = undefined;
        });

        $scope.newMeal = {
            name: undefined,
            ingredients: [],
            receipt: undefined,
            prepareTime: undefined
        };
    };

    $scope.blur = function( $event, $index ){
        $scope.newMeal.ingredients[ $index ] = $event.currentTarget.value;
    };

    $scope.newIngredient = function() {
        $scope.newMeal.ingredients.push("");
    };

    $scope.removeIngredient = function(id) {
        $scope.newMeal.ingredients.splice(id, 1);
    };

    $scope.details = function(id) {
        $location.path('/meal/' + id);
    };

    $scope.addMeal = function() {

        $http.post("/api/meal", JSON.stringify($scope.newMeal)).then(function (response) {
            toastr.success('We added your meal to list :)', 'Success');
            refreshMeals();
        });
    };

    refreshMeals();
});

EatIdeaApp.controller('MealDetailController', function($scope, $http, $routeParams, $location, toastr) {

    $http.get("/api/meal/" + $routeParams.id)
        .then(function mySuccess(response) {
            $scope.meal = response.data;
    }, function myError(response) {
        $scope.meal = undefined;
    });

    $scope.deleteMeal = function(id){
        $http({
            method : "DELETE",
            url : "/api/meal/" + id
        }).then(function mySuccess(response) {
            toastr.success("Usunięto");
            $location.path('/meal');
        }, function myError(response) {

        });
    };

    $scope.vars = {};
    $scope.vars.shoplistName = "";

    $scope.createShoplist = function() {
        $("#selectNameModal").css('display', 'none');
        $http.post("/api/meal/"+$scope.meal.id+"/shoplist?name="+ $scope.vars.shoplistName)
            .then(function (response) {
                toastr.info("Kliknij aby przejść do listy", "Utworzono liste zakupow", {
                    onTap: function() {
                        $location.path('/shoplist/' + response.data);
                    }
                });
            });
    };

    $scope.showNameSelect = function() {
        $("#selectNameModal").css('display', 'block');

    };

    $scope.showShoplists = function () {
        $("#shoplistsModal").css('display', 'block');

        $http.get("/api/shoplist")
            .then(function (response) {
                $scope.ableShoplists = response.data;
            }, function (reason) {
                $scope.ableShoplists = [];
            });
    };

    $scope.addToShoplist = function(id) {
        $("#shoplistsModal").css('display', 'none');
        $http.post("/api/meal/"+$scope.meal.id+"/shoplist?parentShoplistId="+id)
            .then(function (response) {
                toastr.info("Kliknij aby przejść do listy", "Utworzono liste zakupow", {
                    onTap: function() {
                        $location.path('/shoplist/' + response.data);
                    }
                });
            });
    };

});