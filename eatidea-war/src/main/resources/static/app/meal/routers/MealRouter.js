EatIdeaApp.Routers.MealRouter = [
    {
        when:           "/meal",
        templateUrl:	"app/meal/views/meal.html",
        controllerJs:   "app/meal/controllers/MealController.js",
        controller:     "MealController"
    },
    {
        when:           "/meal/:id",
        templateUrl:	"app/meal/views/mealDetail.html",
        controllerJs:   "app/meal/controllers/MealController.js",
        controller:     "MealDetailController"
    }
];