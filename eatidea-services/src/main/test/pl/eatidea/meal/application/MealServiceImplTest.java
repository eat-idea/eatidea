package pl.eatidea.meal.application;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.meal.dao.MealDao;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.meal.model.builder.MealBuilder;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MealServiceImplTest {

	@Mock
	private MealDao mealDao;

	@InjectMocks
	private MealServiceImpl service;

	private Meal returnMealOnId1;
	private List<Meal> returnMeals;

	@Before
	public void init() {
		returnMealOnId1 = MealBuilder.aMeal().withId(1L).withName("test").build();
		given(mealDao.getOne(eq(1L))).willReturn(returnMealOnId1);
		given(mealDao.getOne(eq(2L))).willThrow(new EntityNotFoundException());


		returnMeals = Lists.newArrayList(
			MealBuilder.aMeal().withId(1L).build(),
			MealBuilder.aMeal().withId(2L).build()
		);
		given(mealDao.findAll()).willReturn(returnMeals);
	}

	@Test
	public void returns_meal_by_id() throws ApplicationException {
		//given

		//when
		Meal meal = service.getMeal(1L);

		//then
		assertThat(meal).isEqualTo(returnMealOnId1);
	}

	@Test
	public void returns_all_meals() {
		//given

		//when
		List<Meal> meals = service.getMeals();

		//then
		assertThat(meals).isEqualTo(returnMeals);
	}

	@Test
	public void throws_application_exception_when_not_found_by_id() {
		//given

		//when
		try {
			Meal meal = service.getMeal(2L);
			fail("Should thrown ApplicationException");
		} catch (ApplicationException e) {
			//then
			//ok
		}
	}

	@Test
	public void invocates_save_on_dao() {
		//when
		service.saveMeal(MealBuilder.aMeal().withId(3L).build());

		//then
		verify(mealDao, times(1)).saveAndFlush(any());
	}

	@Test
	public void invocates_delete_on_dao() {
		//when
		service.deleteMeal(1L);

		//then
		verify(mealDao, times(1)).delete(anyLong());
	}

}
