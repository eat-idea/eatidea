package pl.eatidea.shoplist.application;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.meal.dao.MealDao;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.meal.model.builder.MealBuilder;
import pl.eatidea.shoplist.dao.ShoplistDao;
import pl.eatidea.shoplist.model.Shoplist;
import pl.eatidea.shoplist.model.builder.ShoplistBuilder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ShoplistServiceTest {

	@Mock
	private MealDao mealDao;

	@Mock
	private ShoplistDao shoplistDao;

	@InjectMocks
	private ShoplistService service;

	@Test
	public void returns_shoplist_by_id() throws ApplicationException {
		//given
		final Shoplist testShoplist = ShoplistBuilder.aShoplist().withId(1L).withName("shoplistTest").build();
		when(shoplistDao.getOne(eq(1L))).thenReturn(testShoplist);

		//when
		Shoplist shoplist = service.getShoplist(1L);

		//then
		assertThat(shoplist).isEqualTo(testShoplist);
	}

	@Test
	public void returns_all_shoplists() {
		//given
		final List<Shoplist> testShoplists = Lists.newArrayList(
			ShoplistBuilder.aShoplist().withId(1L).withName("shoplistTest").build());
		when(shoplistDao.findAll()).thenReturn(testShoplists);

		//when
		List<Shoplist> shoplists = service.getShoplists();

		//then
		assertThat(shoplists).isEqualTo(testShoplists);
	}

	@Test
	public void creating_shoplist_for_meal() {
		//given
		final String name = "testShoplist";
		String products = "Test1;Test2";
		Meal meal = MealBuilder.aMeal().withId(1L).withIngredients(products).build();
		when(mealDao.getOne(eq(1L))).thenReturn(meal);

		//when
		Shoplist list = service.createShoplistForMeal(name, 1L);

		//then
		assertThat(list.getName()).isEqualTo(name);
		assertThat(list.getProducts()).isEqualTo(products);
		verify(shoplistDao, times(1)).saveAndFlush(list);
	}

	@Test
	public void adding_meal_products_to_shoplist() {
		//given
		final String shoplistProducts = "Test22;Test33";
		final String mealProducts = "Test1;Test2";
		Meal meal = MealBuilder.aMeal().withId(1L).withIngredients(mealProducts).build();
		Shoplist shoplist = ShoplistBuilder.aShoplist().withId(1L).withProducts(shoplistProducts).build();
		when(mealDao.getOne(eq(1L))).thenReturn(meal);
		when(shoplistDao.getOne(eq(1L))).thenReturn(shoplist);

		//when
		Shoplist newShoplist = service.addMealToShoplist(1L, 1L);

		//then
		assertThat(newShoplist).isEqualTo(shoplist);
		assertThat(newShoplist.getProducts()).isEqualTo(shoplistProducts+";"+mealProducts);
		verify(shoplistDao, times(1)).saveAndFlush(newShoplist);
	}

	@Test
	public void deletes_product_from_shoplist() {
		//given
		final String products = "test1;test2;test3";
		final Shoplist shoplist = ShoplistBuilder.aShoplist().withId(1L).withProducts(products).build();
		when(shoplistDao.getOne(eq(1L))).thenReturn(shoplist);

		//when
		Shoplist shoplist1 = service.removeProductFromShoplist(1L, "test2");

		//then
		assertThat(shoplist1.getProducts()).contains("test1");
		assertThat(shoplist1.getProducts()).doesNotContain("test2");
		assertThat(shoplist1.getProducts()).contains("test3");
		verify(shoplistDao, times(1)).saveAndFlush(shoplist1);
	}

}
