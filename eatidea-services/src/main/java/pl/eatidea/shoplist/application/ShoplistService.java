package pl.eatidea.shoplist.application;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.eatidea.meal.dao.MealDao;
import pl.eatidea.meal.model.Meal;
import pl.eatidea.shoplist.dao.ShoplistDao;
import pl.eatidea.shoplist.model.Shoplist;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Shoplist service
 * services for handling with shoplist
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@Service
public class ShoplistService {

    @Autowired
    MealDao mealDao;

    @Autowired
    ShoplistDao shoplistDao;

    /**
     * returns all shoplist in DB
     * @return
     */
    public List<Shoplist> getShoplists() {
        return shoplistDao.findAll();
    }

    /**
     * returns shoplist with given id
     * @param id
     * @return
     */
    public Shoplist getShoplist(Long id) {
        return shoplistDao.getOne(id);
    }

    /**
     * adding ingredients from meal to shoplist
     * @param shoplistId
     * @param mealId
     * @return
     */
    public Shoplist addMealToShoplist(Long shoplistId, Long mealId) {
        Meal meal = mealDao.getOne(mealId);
        Shoplist shoplist = shoplistDao.getOne(shoplistId);
        String products = shoplist.getProducts();
        products += ";" + meal.getIngredients();
        shoplist.setProducts(products);
        shoplistDao.saveAndFlush(shoplist);
        return shoplist;
    }

    /**
     * creating shoplist with given name of meal ingredients
     * @param name
     * @param mealId
     * @return
     */
    public Shoplist createShoplistForMeal(String name, Long mealId) {
        Meal meal = mealDao.getOne(mealId);
        name = name == null ? meal.getName() + " shoplist" : name;
        Shoplist shoplist = new Shoplist();
        shoplist.setName(name);
        shoplist.setProducts(meal.getIngredients());
        shoplistDao.saveAndFlush(shoplist);
        return shoplist;
    }

    public Shoplist removeProductFromShoplist(Long shoplistId, String product) {
        Shoplist shoplist = shoplistDao.getOne(shoplistId);
        String[] products = shoplist.getProducts().split(";");
        shoplist.setProducts(
            String.join(";",
                Lists.newArrayList(products).stream().filter(p -> !product.equalsIgnoreCase(p)).collect(Collectors.toList())));
        shoplistDao.saveAndFlush(shoplist);
        return shoplist;
    }

}
