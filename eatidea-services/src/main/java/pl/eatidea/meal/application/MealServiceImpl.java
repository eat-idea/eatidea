package pl.eatidea.meal.application;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.meal.dao.MealDao;
import pl.eatidea.meal.model.Meal;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Meal Service Impl
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@Component
public class MealServiceImpl implements MealService {

    @Autowired
    MealDao mealDao;

    @Autowired
    Logger logger;

    @Override
    public List<Meal> getMeals() {
        return mealDao.findAll();
    }

    @Override
    public Meal getMeal(Long id) throws ApplicationException {
        try {
            return mealDao.getOne(id);
        }
        catch(EntityNotFoundException ex) {
            logger.error("Pozycja o id %d nie została znaleziona", id);
            throw new ApplicationException("Pozycja o id " + id + " nie została znaleziona", ex);
        }
    }

    @Override
    public void saveMeal(Meal meal) {
        mealDao.saveAndFlush(meal);
    }

    @Override
    public void deleteMeal(Long id) {
        mealDao.delete(id);
    }
}
