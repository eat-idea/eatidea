package pl.eatidea.meal.application;

import org.springframework.stereotype.Component;
import pl.eatidea.common.ApplicationException;
import pl.eatidea.meal.model.Meal;

import java.util.List;

@Component
public interface MealService {

	/**
	 * Returns List of meals in DB
	 * @return
	 */
	List<Meal> getMeals();

	/**
	 * Returns Meal with given ID
	 * @param id
	 * @return
	 */
	Meal getMeal(Long id) throws ApplicationException;


	/**
	 * Saving given MealDto to DB
	 * @param dto
	 */
	void saveMeal(Meal dto);

	/**
	 * Deletes Meal with given id from DB
	 * @param id
	 */
	void deleteMeal(Long id);
}
