package pl.eatidea.common;

/**
 * Application exception
 * For informing about business exceptions
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public class ApplicationException extends Exception {

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
