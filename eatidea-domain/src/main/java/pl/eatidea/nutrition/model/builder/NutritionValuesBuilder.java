package pl.eatidea.nutrition.model.builder;

import pl.eatidea.nutrition.model.NutritionValues;

public final class NutritionValuesBuilder {
	private Long id;
	private Double calories;
	private Double fat;
	private Double proteins;
	private Double carbohydrates;

	private NutritionValuesBuilder() {
	}

	public static NutritionValuesBuilder aNutritionValues() {
		return new NutritionValuesBuilder();
	}

	public NutritionValuesBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public NutritionValuesBuilder withCalories(Double calories) {
		this.calories = calories;
		return this;
	}

	public NutritionValuesBuilder withFat(Double fat) {
		this.fat = fat;
		return this;
	}

	public NutritionValuesBuilder withProteins(Double proteins) {
		this.proteins = proteins;
		return this;
	}

	public NutritionValuesBuilder withCarbohydrates(Double carbohydrates) {
		this.carbohydrates = carbohydrates;
		return this;
	}

	public NutritionValues build() {
		NutritionValues nutritionValues = new NutritionValues();
		nutritionValues.setId(id);
		nutritionValues.setCalories(calories);
		nutritionValues.setFat(fat);
		nutritionValues.setProteins(proteins);
		nutritionValues.setCarbohydrates(carbohydrates);
		return nutritionValues;
	}
}
