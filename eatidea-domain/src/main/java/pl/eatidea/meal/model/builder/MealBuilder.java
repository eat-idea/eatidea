package pl.eatidea.meal.model.builder;

import pl.eatidea.meal.model.Meal;
import pl.eatidea.nutrition.model.NutritionValues;

public final class MealBuilder {
	private Long id;
	private String name;
	private String ingredients;
	private String receipt;
	private Integer prepareTime;
	private NutritionValues nutritionValues;

	private MealBuilder() {
	}

	public static MealBuilder aMeal() {
		return new MealBuilder();
	}

	public MealBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public MealBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public MealBuilder withIngredients(String ingredients) {
		this.ingredients = ingredients;
		return this;
	}

	public MealBuilder withReceipt(String receipt) {
		this.receipt = receipt;
		return this;
	}

	public MealBuilder withPrepareTime(Integer prepareTime) {
		this.prepareTime = prepareTime;
		return this;
	}

	public MealBuilder withNutritionValues(NutritionValues nutritionValues) {
		this.nutritionValues = nutritionValues;
		return this;
	}

	public Meal build() {
		Meal meal = new Meal();
		meal.setId(id);
		meal.setName(name);
		meal.setIngredients(ingredients);
		meal.setReceipt(receipt);
		meal.setPrepareTime(prepareTime);
		meal.setNutritionValues(nutritionValues);
		return meal;
	}
}
