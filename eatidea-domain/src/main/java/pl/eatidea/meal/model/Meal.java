package pl.eatidea.meal.model;

import pl.eatidea.nutrition.model.NutritionValues;

import javax.persistence.*;

/**
 * Meal entity
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
@Entity
public class Meal {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = 1000)
    private String ingredients;

    @Column(nullable = false, length = 1000)
    private String receipt;

    @Column
    private Integer prepareTime;

    @OneToOne
    @JoinColumn(name = "nutrition_id")
    private NutritionValues nutritionValues;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public Integer getPrepareTime() {
        return prepareTime;
    }

    public void setPrepareTime(Integer prepareTime) {
        this.prepareTime = prepareTime;
    }

    public NutritionValues getNutritionValues() {
        return nutritionValues;
    }

    public void setNutritionValues(NutritionValues nutritionValues) {
        this.nutritionValues = nutritionValues;
    }
}
