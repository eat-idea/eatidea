package pl.eatidea.meal.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.eatidea.meal.model.Meal;

/**
 * Meal Data Access Object
 */
public interface MealDao extends JpaRepository<Meal, Long> {

}