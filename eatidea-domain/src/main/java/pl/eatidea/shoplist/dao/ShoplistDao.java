package pl.eatidea.shoplist.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.eatidea.shoplist.model.Shoplist;

/**
 * Shoplist Data Access Object
 *
 * @author Kacper Kapłon
 * @version 0.0.1
 */
public interface ShoplistDao extends JpaRepository<Shoplist, Long> {

}
